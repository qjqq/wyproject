package com.oaker.hours.doman.entity;


import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("mh_archive_hour")
public class MhArchiveHour {


    /**
     * 存档id
     */
    private Long archiveId;

    /**
     * 工时id
     */
    private Long hourId;
}
