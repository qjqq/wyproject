package com.oaker.hours.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.oaker.common.config.AppConfig;
import com.oaker.common.core.domain.entity.SysUser;
import com.oaker.common.utils.DateUtils;
import com.oaker.hours.config.CostConfig;
import com.oaker.hours.doman.columns.Columns;
import com.oaker.hours.doman.dto.MhReviewDTO;
import com.oaker.hours.doman.dto.ProjectSubHour;
import com.oaker.hours.doman.dto.UserHourSaveDTO;
import com.oaker.hours.doman.entity.MhUserHour;
import com.oaker.hours.doman.vo.UserHourDetailVO;
import com.oaker.hours.doman.vo.UserHourReviewVO;
import com.oaker.hours.mapper.MhUserHourMapper;
import com.oaker.hours.service.MhReviewService;
import com.oaker.system.domain.SysMessage;
import com.oaker.system.domain.SysPost;
import com.oaker.system.service.ISysMessageService;
import com.oaker.system.service.ISysUserService;
import com.oaker.system.service.SysHolidayService;
import com.oaker.system.service.impl.SysPostServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.oaker.common.enums.ReviewType;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class MhReviewServiceImpl  extends ServiceImpl<MhUserHourMapper, MhUserHour> implements MhReviewService {

    @Autowired
    private ISysUserService userService;
    @Resource
    private MhCostServiceImpl mhCostService;
    @Resource
    private MhUserHourServiceImpl userHourService;

    @Resource
    private SysPostServiceImpl postService;

    @Resource
    private ProjectServiceImpl projectService;

    @Resource
    private SysHolidayService sysHolidayService;

    @Resource
    private ISysMessageService sysMessageService;


    @Override
    public void setReview(long resultId,UserHourSaveDTO userHourSaveDTO) {


        //如果没有开启审核
//        if (!AppConfig.WORK_REVIEWS) {
//            if (CostConfig.POST_COST || CostConfig.USER_COST) {
//                mhCostService.create(userHourSaveDTO);
//            }
//        } else {
//            //如果开启审核
//            mhUserHour.setReviewStatus(1);
//        }

        if (AppConfig.WORK_REVIEWS) {
            MhUserHour mhUserHour =  new MhUserHour();
            mhUserHour.setId(resultId);
            mhUserHour.setReviewStatus(1);
            baseMapper.updateById(mhUserHour);
        }

    }


    public  boolean  rejectReview(Long hourId) {
        MhUserHour mhUserHour =  new MhUserHour();
        mhUserHour.setId(hourId);
        mhUserHour.setReviewStatus(-1);
        Integer res = baseMapper.updateById(mhUserHour);
        if (res != 1)  {
            return  false;
        }
        sendReviewMessage(mhUserHour,ReviewType.REJECT);
        return  true;
    }


    public  boolean auditReview(Long hourId){
        MhUserHour mhUserHour = baseMapper.selectById(hourId);
        mhUserHour.setId(hourId);
        mhUserHour.setReviewStatus(2);

        Integer integer = baseMapper.updateById(mhUserHour);
        sendReviewMessage(mhUserHour,ReviewType.PASS);
        return integer >0;




    }

    @Override
    public List<UserHourReviewVO> query(MhReviewDTO mhReviewDTO, LocalDate localDate) {
        List<UserHourReviewVO> voList = new ArrayList<>();
        List<SysUser> userList = userService.selectUserList(mhReviewDTO);
        if (CollectionUtils.isEmpty(userList)) {
            return voList;
        }
        userList.forEach(sysUser -> {
            Long userId = sysUser.getUserId();
            Long postId = postService.selectPostListByUserId(userId);
            EntityWrapper<MhUserHour> wrapper = new EntityWrapper<>();
            if (localDate !=null) {
                wrapper.eq(Columns.MhUserHour.fillDate, localDate);
            }
            if (mhReviewDTO.getReviewStatus() !=null) {
                wrapper.eq(Columns.MhUserHour.reviewStatus, mhReviewDTO.getReviewStatus());
            }
            wrapper.eq(Columns.MhUserHour.userId,userId);
            List<MhUserHour> mhUserHoursList = userHourService.selectList(wrapper);
            for (MhUserHour mhHours: mhUserHoursList) {
                UserHourReviewVO userHourReviewVO = new UserHourReviewVO();
                BeanUtils.copyProperties(mhHours,userHourReviewVO);
                if (!Objects.isNull(postId)) {
                    SysPost sysPost = postService.selectPostById(postId);
                    userHourReviewVO.setPostName(sysPost.getPostName());
                }
                userHourReviewVO.setUserName(sysUser.getUserName());
                userHourReviewVO.setNickName(sysUser.getNickName());
                userHourReviewVO.setDayType(sysHolidayService.isAHoliday(mhHours.getFillDate())?0:1);
                userHourReviewVO.setFillTime(mhHours.getCreateTime());
                voList.add(userHourReviewVO);
            }
        });
        //	numList = numList.stream().sorted(Comparator.comparing(User::getAge).reversed()).collect(Collectors.toList());
        List<UserHourReviewVO> collect = voList.stream().sorted(Comparator.comparing(UserHourReviewVO::getFillDate).reversed()).collect(Collectors.toList());
        return collect;

    }


    public  UserHourReviewVO queryById (int postId) {
        UserHourReviewVO userHourReviewVO = new UserHourReviewVO();

        MhUserHour mhUserHour = userHourService.selectById(postId);

        Long userId = mhUserHour.getUserId();

        SysUser sysUser = userService.selectUserById(userId);
        BeanUtils.copyProperties(mhUserHour,userHourReviewVO);

        userHourReviewVO.setDayType(sysHolidayService.isAHoliday(mhUserHour.getFillDate())?0:1);
        userHourReviewVO.setNickName(sysUser.getNickName());
        return userHourReviewVO;
    }

    private void sendReviewMessage(MhUserHour mhUserHour, ReviewType reviewType) {
        SysMessage msg = new SysMessage();
        String resString ="";
        msg.setTitle("审核");
        msg.setReceiveUserid(mhUserHour.getUserId());
        msg.setSourceId(mhUserHour.getId());
        msg.setCreateTime(DateUtils.getNowDate());
        if (reviewType == ReviewType.PASS) {
            resString ="已审核通过";
        } else {
            resString ="被驳回";
        }
        msg.setMessageContent(""+ mhUserHour.getFillDate()+ "日的日志"+resString );
        sysMessageService.insertHourReviewMessage(msg);

    }
}