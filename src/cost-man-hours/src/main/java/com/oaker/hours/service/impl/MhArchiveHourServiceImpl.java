package com.oaker.hours.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.oaker.hours.doman.entity.MhArchiveHour;
import com.oaker.hours.mapper.MhArchiveHourMapper;
import com.oaker.hours.service.MhArchiveHourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MhArchiveHourServiceImpl  implements MhArchiveHourService {
    @Autowired
    private MhArchiveHourMapper mapper;

    @Override
    public void saveBatch(List<MhArchiveHour> mhArchiveHourList) {
        mapper.insertBatch(mhArchiveHourList);
    }

    @Override
    public List<MhArchiveHour> queryByArchiveId(Long archiveId) {

        //List<MhArchiveHour> list = mapper.queryByArchiveId(archiveId);

        return mapper.queryByArchiveId(archiveId);
    }
}
